package cn.liuda066.www.method;

/**
 * 可变参数
 * JDK1.5开始，Java支持传递同类型的可变参数给一个方法
 * 在方法声明中，在指定参数类型后加一个省略号（...）
 * 一个方法中只能指定一个可变参数，他必须是方法的最后一个参数，任何普通的参数必须在他之前声明
 */
public class CanChangeArguments {
    public static void main(String[] args) {
        CanChangeArguments canChangeArguments = new CanChangeArguments();
        canChangeArguments.test(1,2,3,4,5);
    }
    public void test(int x,int... i){
        for (int j = 0; j <= 4; j++) {
            System.out.println(i[j]);
        }

    }
}

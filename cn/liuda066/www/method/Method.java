package cn.liuda066.www.method;

/**
 * 类似C的函数
 * 方法：解决一类问题的步骤的有序集合
 * 方法包含于类或对象中
 * 方法在程序中创建，在其他地方被引用
 *
 * 一个方法只完成一个功能   ---->原子性
 */

/**
 * 方法包含一个方法头和一个方法体
 * 修饰符 返回值类型 方法名（参数类型 参数名）｛
 *      ...
 *      方法体
 *      ...
 *      return 返回值;
 *      }
 */
public class Method {

    //main方法
    public static void main(String[] args) {
        //实际参数:实际调用传递给他的参数
        //int sum =add(11,2);
        //System.out.println(sum);
        int max = max(10,10);
        System.out.println(max);
    }


    //加法
    //形参,用来定义作用的
    public static int add(int x , int y){
        return x+y;
    }


    //比大小
    public static int max(int num1,int num2){
        int result = 0;
        if(num1==num2){
            //System.out.println("相等");
            return 0;//终止方法
        }else if(num1>num2){
            result = num1;
        }else{
            result = num2;
        }
        return result;
    }
}

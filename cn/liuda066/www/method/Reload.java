package cn.liuda066.www.method;

/**
 *  重载
 *  重载就是在一个类中，有相同的函数名称，但形参不同的函数
 *
 *  规则：
 *      方法名必须相同
 *      参数列表必须不同（个数不同，或类型不同，参数排列顺序不同等）
 *      方法的返回类型可以相同也可以不相同
 *      仅仅返回类型不同不足以成为方法的重载
 *
 *  实现理论：
 *      方法名称相同的时候，编译器会根据调用的方法的参数个数，参数类型等去逐个匹配，以选择对应的方法，如果匹配失败，则编译器报错
 *      JVM匹配
 */
public class Reload {
    public static void main(String[] args) {

    }

    //max 的重载
        //int max
        public static int max ( int num1, int num2){
        int result = 0;
        if (num1 == num2) {
            //System.out.println("相等");
            return 0;//终止方法
        } else if (num1 > num2) {
            result = num1;
        } else {
            result = num2;
        }
        return result;
    }

        //double max
        public static double max ( double num1, double num2){
        double result = 0;
        if (num1 == num2) {
            //System.out.println("相等");
            return 0;//终止方法
        } else if (num1 > num2) {
            result = num1;
        } else {
            result = num2;
        }
        return result;
    }//参数类型
        public static double max ( int num1, int num2,int num3){
        double result = 0;
        if (num1 == num2) {
            //System.out.println("相等");
            return 0;//终止方法
        } else if (num1 > num2) {
            result = num1;
        } else {
            result = num2;
        }
        return result;
    }//参数个数
        //参数顺序
        public static double max ( int num1, double num2){
        double result = 0;
        if (num1 == num2) {
            //System.out.println("相等");
            return 0;//终止方法
        } else if (num1 > num2) {
            result = num1;
        } else {
            result = num2;
        }
        return result;
    }
        public static double max ( double num1,int  num2){
        double result = 0;
        if (num1 == num2) {
            //System.out.println("相等");
            return 0;//终止方法
        } else if (num1 > num2) {
            result = num1;
        } else {
            result = num2;
        }
        return result;
    }

    //add 的重载
        //参数类型
        //int add
        public static int add(int x , int y){
            return x+y;
        }
        //double add
        public static double add(double x , double y){
        return x+y;
    }

        //参数排列顺序
        //double int add
        public static int add(double x , int y){
        return (int)x+y;
    }
        //int double add
        public static int add(int x , double y) {
             //return x +(int) y;
               return (int)(x +y);//二选一
    }

        //参数个数
        public static int add(int x , int y,int z){
            return x+y+z;
        }

}

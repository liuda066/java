package cn.liuda066.www.method;

/**
 * 常量（Constant）:初始化（initialize）后不能改变值，不会变动的值
 *
 * final 常量名 = 值 ；
 * final double PI = 3.14;
 *
 * 常量名一般使用大写字符
 */

/**
 * 命名规范：见名知意
 * 类成员变量：首字母小写 和 驼峰原则 ：monthSalary    除了第一个单词以外，后面的单词首字母大写 lastName
 * 局部变量：首字母小写 和 驼峰原则
 * 常量：大写字母和下划线：MAX_VALUE
 * 类名：首字母大写 和 驼峰原则 ：GoodMan
 * 方法名：首字母小写 和 驼峰原则 ： runRun（）｛｝
 */
public class Constant {

    //修饰符不存在先后顺序
    final static double PI =3.14;
    public static void main(String[] args) {
        System.out.println(PI);
    }
}

package cn.liuda066.www.method;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        manu();
        System.out.print("请输入您想进行的运算：");
        int choice = 0;
        int num1= 0;
        int num2 =0;
        if(s.hasNextInt()){
        choice = s.nextInt();
        }else{
            System.out.print("请输入您想进行的运算：");
        }

        switch (choice){
            case 1:
                System.out.println("请输入两个数：");
                    num1 = s.nextInt();
                    num2 = s.nextInt();
                System.out.println("求和结果为:"+add(num1,num2));
                break;
            case 2:
                System.out.println("请输入两个数：");
                    num1 = s.nextInt();
                    num2 = s.nextInt();
                System.out.println("求差结果为:"+sub(num1,num2));
                break;
            case 3:
                System.out.println("请输入两个数：");
                    num1 = s.nextInt();
                    num2 = s.nextInt();
                System.out.println("求乘结果为:"+mul(num1,num2));
                break;
            case 4:
                System.out.println("请输入两个数：");
                    num1 = s.nextInt();
                    num2 = s.nextInt();
                System.out.println("求除结果为:"+div(num1,num2));
                break;
            case 0:
                    return ;
            default:
                System.out.println("请输入正确选项！");
        }


    }
    //add
    public static int add(int num1,int num2){
        return num1+num2;
    }
    public static double add(int num1,double num2){
        return (double)num1+num2;
    }
    public static double add(double num1,int num2){
        return num1+(double)num2;
    }
    public static double add(double num1,double num2){
        return num1+num2;
    }
    //sub
    public static int sub(int num1,int num2){
        return num1 - num2;
    }
    public static double sub(int num1,double num2){
        return (double)num1-num2;
    }
    public static double sub(double num1,int num2){
        return num1-(double)num2;
    }
    public static double sub(double num1,double num2){
        return num1-num2;
    }
    //mul
    public static int mul(int num1,int num2){
        return num1 * num2;
    }
    public static double mul(int num1,double num2){
        return (double)num1*num2;
    }
    public static double mul(double num1,int num2){
        return num1*(double)num2;
    }
    public static double mul(double num1,double num2){
        return num1*num2;
    }
    //div
    public static double div(int num1,int num2){
        return (double)num1 /(double) num2;
    }
    public static double div(int num1,double num2){
        return (double)num1/num2;
    }
    public static double div(double num1,int num2){
        return num1/(double)num2;
    }
    public static double div(double num1,double num2){
        return num1/num2;
    }
    public static void manu(){
        System.out.println("-----------------------------");
        System.out.println("---       简易计算器       ---");
        System.out.println("-----------------------------");
        System.out.println("---1.加法                  ---");
        System.out.println("---2.减法                  ---");
        System.out.println("---3.乘法                  ---");
        System.out.println("---4.除法                  ---");
        System.out.println("---0.退出                  ---");
        System.out.println("-----------------------------");
    }
}

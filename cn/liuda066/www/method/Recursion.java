package cn.liuda066.www.method;

/**
 * 递归
 * A方法调用A方法 ， 自己调用自己
 *
 * 利用递归可以用简单的程序解决一些复杂的问题。
 * 通常把一个大型复杂的问题层层转换为一个与原问题相似的规模较小的问题来求解
 * 递归策略只需少量的程序就可以描述出解题过程所需要的多次重复计算，大大减少程序的代码量
 * 有限的语句 定义 对象的无限集合
 *
 * 包括：
 *      递归头：什么时候不调用自身的方法。如果没有头，将陷入死循环。
 *      递归体：什么时候需要调用自身方法
 *
 *     //边界条件：调用的最后的一次，结束调用的条件 --->  if(num == 1 )
 *     //前阶段：持续调用阶段 ---->  num*factorial(num-1)
 *     //返回阶段：调用到边界，层层返回值的阶段 -----> return
 *
 *
 *     //java 都是 栈机制 ---->深度越大，占用的时间空间开销越大
 *     //小计算  递归
 *     //大计算   不适用
 */
public class Recursion {
    public static void main(String[] args) {
        Recursion recursion = new Recursion();
        //recursion.test();

        System.out.println(recursion.factorial(12));
    }

    public void test(){//StackOverflowError
        test();
    }
    //阶乘
    public int factorial(int num){
        if(num == 1 ){
            return 1;
        }else{
            return num*factorial(num-1);
        }
    }
}

package cn.liuda066.www.base;
/**break and continue
       //break
            在任何循环语句的主体部分，均可用break控制循环流程
            break用于 强行退出循环 ，不执行循环中剩余的语句。（switch语句中也使用）
            终止整个循环

       //continue
            用于循环语句体中，用于终止 某次 循环
            跳过循环体中尚未执行的语句，接着进行下一次是否执行循环的判定
            跳过某一次循环

       //关于goto关键字
            break和continue    是 带标签的 goto
            标签：是指后面带一个冒号的标识符，例如label:

       //区别：
            1.break在循环语句中的任何主体部分，均可以控制循环流程
            2.break用于强制退出循环，不需要执行循环剩余的语句。（break也在switch中使用）
            3.continue 语句再循环语句体中，用于终止某次循环过程，即跳过循环体为执行的语句，接着进行下一次是否执行循环的判定
 */

//braek
public class BAndC {
    public static void main(String[] args) {
       // break1();
        continue1();
    }

    //break
    public static void break1() {
        int i = 0;
        //循环
        while (i < 100) {
            i++;
            System.out.println(i);
            if (i == 30) {
                break;
            }

        }
        System.out.println("123");
        //switch语法


    }

    //continue
    public static void continue1() {
        int i = 0;
        //循环
        while (i < 100) {
            i++;
            if (i % 10 == 0) {
                System.out.println();
                continue;
            }
            System.out.print(i);
        }
    }

    //goto
    public  static void label(){
        //打印101~150之间的所有质数

        //质数，在大于1的自然数中，除了1和它本身以外不再有其他因数的自然数

        int count =0 ;
        outer:for(int i = 101;i<150;i++){
            for(int j =2;j<i/2;i++){
                if(i%j==0){
                    continue outer;
                }
            }
        }
    }
}
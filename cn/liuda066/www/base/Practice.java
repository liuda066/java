package cn.liuda066.www.base;

public class Practice {
    public static void main(String[] args) {

        for(int i = 1;i<10;i++){
            for(int j = 1;j<=i;j++){
                System.out.print(j+" * "+ i +" = "+i*j +"\t" );
            }
            System.out.println();
        }

    }
    //增强for循环
    public static void forPlus(){
        int[] numbers  ={10,20,30,40,50};
        for(int x:numbers){
            System.out.println(x);
        }
    }
}

package cn.liuda066.www.base;

import java.util.Scanner;

public class ScannerExercise {
    public static void main(String[] args) {
        exercise();
    }

    public static void exercise(){
        int i =0;
        Scanner scanner=  new Scanner(System.in);

        //和
        double sum=0;
        //计算输入了多少个数字
        int m = 0;
        //通过循环判断是否还有输入，并在里面对每一次进行求和统计
        System.out.println("请输入您的数据：");
        while(scanner.hasNextDouble()){
            double x =scanner.nextDouble();
            m++;
            sum = sum + x;
            System.out.println("你输入的第"+m+"个数据，然后当前结果sum="+sum);
        }

        System.out.println(m+"个数的和为"+sum);
        System.out.println(m+"个数的平均值为"+sum / m);

        scanner.close();
    }
}

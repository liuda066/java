
package cn.liuda066.www.base;//包下，必须再类前声明

import com.company.*;//必须放在package语句之下

public class Package {
    public static void main(String[] args) {

    }
    public static void package1(){
        //用于区别类名的命名空间 相当于文件夹
        /*
        语法格式：package pkg1[.pkg2[.okg3...]];

        一般利用公司域名倒置作为包名；
        使用import导包
        import package1[.package2...].(classname|*);


        package cn.liuda066.www;//包下，必须再类前声明

        import com.company.*;//必须放在package语句之下


         */
    }
}

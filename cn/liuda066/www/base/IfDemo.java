package cn.liuda066.www.base;

import javax.xml.stream.events.EndDocument;
import java.util.Scanner;

public class IfDemo {
    public static void main(String[] args) {
        switch1();
    }
    //单选择结构if
    public static void ifDemo1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入内容：");
        String s = scanner.nextLine();

        //equals:判断字符串是否相等
        if(s.equals("hello")){
            System.out.println(s);
        }
        System.out.println("End");

        scanner.close();
    }

    //双选择结构 if...else...
    public static void ifDemo2(){
        //考试分数大于60就是及格，小于60分就不及格。
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入成绩：");

        int score = scanner.nextInt();
        if(score>=60){
            System.out.println("您及格了。");
        }else{
            System.out.println("您挂科了。");
        }

        scanner.close();
    }
    //if多选择结构
    public static void ifDemo3(){
        Scanner scanner = new Scanner(System.in);

        /*
            if语句之多有1个else语句，else在else if 语句之后。
            只要有一个else if 为 true ，其他的则都跳过执行
         */
        System.out.println("请输入您的成绩：");
        int score = scanner.nextInt();
        if(score>=90&&score<=100){
            System.out.println("优秀！");
        }else if(score>=80&&score<90){
            System.out.println("良好！");
        }else if(score>=60&&score<80){
            System.out.println("及格！");
        }else if(score>=0&&score<60){
            System.out.println("寄了！");
        }else {
            System.out.println("成绩输入不合法！");
        }
    }
    //if嵌套结构

    //switch
    //JDK7新特性，表达式结果可以是字符串！！！
    //自负的本质还是数字
    //反编译 java ---- class(字节码文件) --- 反编译（idea）
    //不用break 造成case穿透现象  Switch  匹配
    public static void switch1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的名字：");
        String name = scanner.nextLine();
        switch (name){
            case "菜菜小蒙":
                System.out.println("不是牛牛！是小蒙");
                break;
            case "菜菜小ya":
                System.out.println("不是牛牛！是小ya");
                break;
            case "六道牛牛":
                System.out.println("猜对了！");
                break;
            default:
                System.out.println("都错啦！");

        }
        scanner.close();
    }
}

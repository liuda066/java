package cn.liuda066.www.base;

/**@Liuda066
 * 本项目主要 学习实践：
 * 注释
 * 标识符
 * 关键字
 * 数据类型
 * date:2022.04.03
 */
public class DataType {
    public static void main(String[] args){
        dataTypePlus();
    }
    public static void notes(){
        //单行注释
        /*
          多行注释
          多行注释
         */
        /**
         * 文档注释
         */
    }
    public static void identifier(){
        //标识符
        //不可以用关键字做标识符
        //关键字：break,const,case,class,interface,public,static,void,new,goto,int,float.double....
        //以字母,美元符$,下划线_开始,首字符之后可以是 字母，美元符$,下划线_,或者数字组合.....
        //合法举例:age,&salary,_value,__1_value.....
        //非法举例:123abc,-salary,#abc......
        //可以使用中文名，但是不建议，也不建议用拼音。
        //大小写敏感！
    }
    public static void datatype(){
        //数据类型
        //强类型语言：所有变量 先定义后使用，若不修改，则保持原有数据类型
        //弱类型语言：可以不符合规定，随便玩。javascript....
        //两类数据类型：
        //              基础数据类型：byte 1 ,short 2 ,int 4 ,long 8 ,float 4 ,double 8 ,char 2 ,boolean 1 (true,false)
        //              引用数据类型：类 class，接口 interface, 数组 array...
        //整形
        byte num1 = 127;
       // byte b = 128;
       // byte b =-129;   -128~127
        short num2 = 12321;
        int num3 = 1231231232;
        long num4 = 1111111111132131232L;//注意long类型后面要加 L;

        //浮点型
        float num5 = 12.3F;//注意float类型后面要加 F;
        double num6 = 3.141592653589793284626431;

        //字符
        char word1 = 'l';
        char word2 = '牛';
        //字符串，String不是关键字，类
        //String name ="六道牛牛";

        //布尔类型
        boolean flag_t = true;
        boolean flag_f = false;

        //什么是字节？
        //bit 位，byte字节，1byte = 8 bit , 32 -> 4GB内存，64 -> 很大很大，2的64次方/1024/1024/1024....
    }
    public static void dataTypePlus(){
        //整数拓展：   进制 二进制0b,   十进制    ， 八进制0   十六进制0x
        int i = 10;
        int i8 = 010;
        int i16 = 0x10;
        int i2 = 0b10;
        System.out.println("十进制 = "+i);
        System.out.println("八进制 = "+i8);
        System.out.println("十六进制 = "+i16);//0~9,A~F
        System.out.println("二进制 = "+i2);
        System.out.println("-----------------------------------------");
        //-----------------------------------------
        //浮点数拓展： 银行业务表示问题  ------>BigDecimal 数学工具类

        //float       精度有限 离散的 摄入误差 接近但不等于
        //double
        //最好完全避免使用浮点数进行比较

        float f = 0.1f;
        double d = 1.0/10;
        System.out.println(f==d);
        System.out.println(f);
        System.out.println(d);

        float f1 = 1231231232131f;
        float f2 = f1+1;
        System.out.println(f1==f2);
        System.out.println("-------------------------");

        //字符拓展

        char c1 = 'a';
        char c2 = '中';
        System.out.println(c1);
        System.out.println(c2);
        System.out.println((int)c1);//强制类型转换
        System.out.println((int)c2);
        //所有的字符本质都是数字
        //编码 Unicode 2字节  65536   excel 2的16次 = 65536；
        //Unicode 表 (97 = a ,65 = A)
        //U0000 , UFFFF;
        char c3 = '\u0061';
        System.out.println(c3);

        //转义字符
        // \t 制表符
        // \n 换行
        //.........

        //对象 从内存分析
        String sa = new String("hello world");
        String sb = new String("hello world");
        System.out.println(sa==sb);
        String sc = ("hello world");
        String sd = ("hello world");
        System.out.println(sc==sd);

        //布尔值扩展
        boolean flag =true;
        if(flag == true){
            System.out.println("牛牛最帅");
        }
        if(flag){
            System.out.println("牛牛最帅");
        }
    }

}

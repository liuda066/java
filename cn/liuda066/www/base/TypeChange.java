package cn.liuda066.www.base;

/**@Liuda066
 * 本项目主要 学习实践：
 * 类型转换
 * date:2022.04.03
 */
public class TypeChange {
    public static void main(String[] args) {
        //typeChange();
        matters();
    }
    public static void typeChange(){
        //不同类型的数据先转换为同类型，在进行运算
        int i = 128;
        int j = 127;
        byte b =(byte)i;//内存溢出
        byte c = (byte)j;
        System.out.println(i);
        System.out.println(b);
        System.out.println(c);

        //强制类型转换 (类型)变量名 高 -- 低
        //自动转换                 低到高
        //容量等级（小->大） ： byte,short,char<int<long<float<double

        /*
        注意点：
        1.不能对布尔值进行转换
        2.不能把对象类型转换为不相干的类型
        3.在把高容量转换为低容量的时候，强制类型转换
        4.转换的时候可能出现内存溢出或者精度问题！
         */
        System.out.println((int)23.23);
        System.out.println((int)-45.23f);
        System.out.println("-------------------------------");
        char ch ='a';
        int d = ch+1;
        System.out.println(d);
        System.out.println((char)d);
    }
    public static void matters(){
        //操作比较大的数的时候，注意溢出问题
        //JDK7新特性,数字之间可以用下划线分割
        int money = 10_0000_0000;
        int years = 20;
        int total1 = money*years;
        long total2 = money*years;//计算之前已经存在问题了
        long total3 = money*((long)years);//想把一个数转换为long，在运算的时候自动提升
        long total4 = (long)(money*years);//整体强转没作用，计算之前已经存在问题了
        System.out.println(total1);//精度溢出
        System.out.println(total2);
        System.out.println(total3);
        System.out.println(total4);
        double e = Math.pow(2,3);//数学工具类
        System.out.println(e);

    }
}

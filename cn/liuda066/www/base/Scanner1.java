package cn.liuda066.www.base;
import java.util.Scanner;

/**
 * next(): 一定要读取到有效字符后才可以结束输入。
 *         对输入有效字符之前遇到的空白，next()方法会自动将其去掉
 *         只有输入有效字符后才将其后面输入的空白作为分隔符或者结束符
 *         next()不能得到带有空格的字符串
 *
 * nextLine():以Enter为结束符，也就是说nextLine()方法返回的是输入回车之前的所有字符。
 *            可以获取空白
 */
public class Scanner1 {
    public static void main(String[] args) {
    scannerPlus();
    }
    public static void scanner1(){
        //获取用户的输入
        //java.util.Scanner 类
        //通过Scanner类的next()与nextLine()方法获取输入的字符串，
        //在读取前我们一般需要使用hasNext()与hasNextLine()判断是否还有输入的数据S

        //创建一个扫描器对象，用于接受键盘数据
        Scanner scanner = new Scanner(System.in);

        System.out.println("使用next方式接受：");

        //判断用户有没有输入字符串
        if(scanner.hasNext()){
            //使用next方式接收
            String str = scanner.next();//程序等待用户输入
            System.out.println("输入的内容为："+str);
        }

        //凡是属于IO流的类如果不关闭会一直占用资源，用完即关
        scanner.close();
    }
    public static void scanner2(){
        //从键盘接收数据
        Scanner scanner = new Scanner(System.in);
        System.out.println("使用next方式接受：");

        //判断是否还有输入
        if(scanner.hasNextLine()){
            String str = scanner.nextLine();
            System.out.println("输入的内容为："+str);
        }
        scanner.close();
    }
    public static void scannerPlus(){
        //
        Scanner scanner = new Scanner(System.in);

        int i = 0;
        float f = 0.0f;

        //整数
        System.out.println("请输入整数：");
        if(scanner.hasNextInt()){
                i = scanner.nextInt();

            System.out.println("整数数据："+i);
        }else{
            System.out.println("你输入的不是整数！");
        }

        //小数
        System.out.println("请输入小数：");
        if(scanner.hasNextFloat()){
                f = scanner.nextFloat();

            System.out.println("小数数据："+f);
        }else{
            System.out.println("你输入的不是小数！");
        }

        scanner.close();
    }

}

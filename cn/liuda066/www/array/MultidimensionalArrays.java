package cn.liuda066.www.array;

/**
 * 多维数组
 */
public class MultidimensionalArrays {
    public static void main(String[] args) {

        //[4][2]四行两列
        /*
         array[0]   --->1,2
         array[1]   --->2,3
         array[2]   --->3,4
         array[3]   --->4,5
         */
        int[][] array = {{1,2},{2,3},{3,4},{4,5}};//二维数组

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.println(array[i][j]);
            }
        }
        printArray(array[0]);
    }
    public static void printArray(int[] arrays){
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i]+" ");
        }
    }

}

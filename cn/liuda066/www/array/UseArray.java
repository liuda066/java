package cn.liuda066.www.array;

/**
 * 数组的使用
 * 1.打印元素
 * 2.计算所有元素之和
 * 3.查找最大元素
 */
public class UseArray {
    public static void main(String[] args) {
        int[] arrays = {1, 2, 3, 4, 5};

        //打印全部的数组元素
        for (int i = 0; i < arrays.length; i++) {
            System.out.println(arrays[i]);
        }

        //计算所有元素之和
        int sum = 0;
        for (int i = 0; i < arrays.length; i++) {
            sum = sum + arrays[i];  //  sum+=arrays[i];
        }
        System.out.println("sum = "+sum);

        //查找最大元素
        int max = arrays[0];
        for (int i = 1; i <arrays.length ; i++) {
            if(max<arrays[i]){
                max = arrays[i];
            }
        }
        System.out.println("max = "+max);
        System.out.println("-----------------------------------------------------");
        useArrayPlus();
        System.out.println("-----------------------------------------------------");
        printArray(arrays);
        int[] revers = revers(arrays);
        printArray(revers);
    }

    public static void useArrayPlus(){
        /**
         * For-Each 循环
         *  JDK 1.5 没有下标  便于遍历，操作内部数据不便
         */
        int[] arrays = {1,2,3,4,5};

        for (int array : arrays) {
            System.out.println(array);
        }
    }

    /**
     * 数组作为参数
     */
    //打印数组元素
    public static void printArray(int[] arrays){
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i]+" ");
        }
    }
    /**
     * 返回数组
     */
    //反转数组
    public static int[] revers(int[] arrays){
        int[] result = new int[arrays.length];
        //反转的操作

        /*
        for (int i = 0; i < arrays.length; i++) {
            result[arrays.length-i-1]=arrays[i];
        }*/
        //或者：
        for (int i = 0,j=result.length-1; i < arrays.length; i++,j--) {
           result[j]=arrays[i];
        }
        return result;

    }


}

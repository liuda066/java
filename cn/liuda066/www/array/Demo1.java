package cn.liuda066.www.array;

/**
 * 数组的定义(什么是数组)
 *
 * 相同类型数据的有序集合
 * 数组描述的是相同类型的若干个数据，按照一定的先后次序排列组合而成
 * 其中，每一个数据称作一个数组元素，每个数组元素可以通过一个下标来访问
 */

/**
 * 数组的声明和创建
 * //变量的类型 变量的名字 = 变量的值 ；
 * //数组类型  数组名 ;    例：dataType[] arrayRefVar;   dataType arrayRefVar[]
 *
 * java语言使用new操作符来创建数组，语法如下：
 * dataType[] arrayRefVar = new dataType[arraySize];
 *
 * 数组的元素是通过索引访问的，数组索引从0开始;
 * 获取数组长度：
 * arrays.length
 */

/**
 * 数组的特点：
 *          其长度是确定的，一旦被创建，大小不可改变
 *          其元素必须是相同类型，不允许出现混合类型
 *        //数组中的元素可以是任何数据类型，包括基本类型和引用类型
 *        //数组变量属于引用类型，数组也可以看成是对象，数组中的每一个元素相当于该对象的成员变量
 *          数组本身就是对象，java中对象是在堆中的，因此数组无论保存原始类型还是其他对象类型，
 *        //数据对象本身是在堆中的
 *
 */
public class Demo1 {
    public static void main(String[] args) {
        //1.声明一个数组
        int[] nums;  //  首选
        int nums2[]; //效果相同

        //2.创建一个数组
        nums = new int[10];
        int[] num3 = new int[10];//这里面可以存放10个int类型的数字

        //3.给数组元素赋值,默认值为0
        nums[0]=1;
        nums[1]=2;
        nums[2]=3;
        nums[3]=4;
        nums[4]=5;
        nums[5]=6;
        nums[6]=7;
        nums[7]=8;
        nums[8]=9;
        nums[9]=10;

        //计算所有元素的和
        int sum =0 ;
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i];
        }
        System.out.println("总和为："+sum);
    }

}

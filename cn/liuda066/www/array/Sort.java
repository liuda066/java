package cn.liuda066.www.array;

import java.util.Arrays;

/**
 * 冒泡排序
 * 1.比较数组中，两个相邻的元素，如果第一个数比第二个数大，我们就交换他们的位置
 * 2.每一次比较都会产生一个最大，或者最小的数字；
 * 3.下一轮则可以减少一次排序！
 * 4.依次循环直到结束！
 */
public class Sort {
    public static void main(String[] args) {
        int[] a = {2,32,1,3,12,31,1,2,22,2,3,2232,322};
        int[] resurt =sort(a);

        System.out.println(Arrays.toString(resurt)  );
    }

    public static int[] sort(int[] arrays){
        //零时变量
        int temp = 0;

        //外层循环，判断我们要走多少次
        for (int i = 0; i < arrays.length-1; i++) {
            boolean flag = false;
            //内层循环 ，比较判断两个数，如果第一个数比第二个数大，则交换位置。
            for (int j = 0; j < arrays.length-1-i; j++) {//降序排列
                if(arrays[j+1]>arrays[j]){
                    temp=arrays[j+1];
                    arrays[j+1]=arrays[j];
                    arrays[j]=temp;
                    flag = true;
                }
            }
            if(flag==false){
                break;
            }
        }
        return  arrays;
    }
}

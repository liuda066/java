package cn.liuda066.www.array;

/**
 * 内存分析
 *  堆 ->存放new的对象和数组
 *     -->可以被所有的线程共享，不会存放别的对象引用
 *  栈 ->存放基本变量类型（会包含这个基本类型的具体数值）
 *     -->引用对象的变量(会存放这个引用在堆里面的地址)
 *  方法区 -> 可以被所有线程共享
 *        --> 包含了所有的class和static变量
 *
 */

import java.lang.management.ManagementFactory;

/**
 * 数组的三种初始化
 * 1.静态初始化
 * int[] a = {1,2,3,4,5,6};
 *
 */
public class Demo2 {
    public static void main(String[] args) {
        //1.静态初始化
        int[] a = {1,2,3,4,5,6};//基础类型
        Man[] mans = {new Man(),new Man()};//引用类型

        //动态初始化
        int[] b = new int[10];
        b[0]=10;

        //默认初始化
        //数组是引用类型，他的元素相当于类的实例变量，因此数组一经分配空间，其中的每个元素
        //也被按照实例变量同样的方式被隐式初始化

        System.out.println(b[0]);
    }
}

package cn.liuda066.www.array;

import java.util.Arrays;
/**
 * Arrays类
 * 数组工具类 java.util.Arrays
 * 由于数组对象本身没有什么方法可以供我们调用，
 * 但API中提供了一个工具类Arrays供我们使用
 * 从而可以对数据对象进行一些基本操作
 *
 * 查看JDK帮助文档
 *
 *Arrays类中的方法都是static修饰的静态方法，在使用的时候可以直接使用类名进行调用
 * 而“不用”使用对象来调用： （注意：是“不用”而不是“不能”）
 *
 * 具有以下常用功能：
 *      1.给数组赋值：通过fill方法
 *      2.对数组排序：通过sort方法，按升序
 *      3.比较数组：通过equals方法比较数组中元素是否相等。
 *      4.查找数组元素：通过binarySearch方法能对排序好的数组进行二分查找法操作。
 */
public class ArrayPlus {
    public static void main(String[] args){
        demo1();
    }
    public static void demo1(){
        int[] a = {1,2,3,4,9797,4545,33,5,45,54};
        int[] b = {1,2,4,4,9797,4545,33,5,45,54};

        System.out.println(a);// hashcode  [I@4554617c

        //Arrays.toString(int[] arrays)  //打印数组中的元素
        System.out.println(Arrays.toString(a));//打印数组中的元素
        printArrays( a);

        //Arrays.sort(int[] arrays) //数组元素排序类 ：升序

        Arrays.sort(a);
        System.out.println(Arrays.toString(a));


        //Arrays.equals(a,a);  //比较数组元素是否相等   结果返回布尔值
        System.out.println(Arrays.equals(a,b));


        //Arrays.binarySearch(a,4); //二分查找数组中元素等于key的数，返回下标
        System.out.println(Arrays.binarySearch(b,4));


        //Arrays.fill(a,2,4,0);填充数据，从第2个到第4个元素填充为 0 。 “左闭右开” --> [1, 2, 0, 0, 5, 33, 45, 54, 4545, 9797]
        Arrays.fill(a,2,4,0);
        System.out.println(Arrays.toString(a));


    }

    //重复造轮子 ---->打印数组中的元素
    public static void printArrays(int[] arrays){
        for (int i = 0; i < arrays.length; i++) {
            if(i==0){
                System.out.print("["+arrays[i] + ", ");
            }else if(i==arrays.length-1){
                System.out.print(arrays[i]+"]");
            }else {
                System.out.print(arrays[i] + ", ");
            }
        }
        System.out.println();
    }


}

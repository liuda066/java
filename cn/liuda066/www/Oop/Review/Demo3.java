package cn.liuda066.www.Oop.Review;

public class Demo3 {
    //值传递和引用传递
    //栈中不变，堆中变
    public static void main(String[] args) {
        //值传递
//        int a = 1 ;
//        System.out.println(a);//1
//
//        Demo4.change(a);
//
//        System.out.println(a);//1
        //引用传递：传递一个对象,本质还是值传递
        Person person = new Person();
        System.out.println(person.name);

        Demo3.change1(person);
        System.out.println(person.name);

    }
    //返回值为空
    public static void change(int a ){//a传递的是栈中的值，复制一份值给方法，他改变，栈中的值不变（基础类型栈存的值，引用类型存的地址）
        a=10;
    }
    public static void change1(Person person ){//person传递的是堆中的地址，复制一份地址给方法，改变则改变 相当于C中指针传递
        //person 是一个对象：指向的 --> Person person = new Person(); 这是一个具体的人
        person.name = "六道牛牛";
    }
}

//定义一个person类，有一个属性：name
class Person{
    String name; //默认值null
}

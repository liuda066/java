package cn.liuda066.www.Oop.Review;

public class Demo2 {
    public static void main(String[] args) {
        //实际参数和形式参数对应

        //1.new
        int add = new Demo2().add(1,2);
        //2.static
        int add1 = Demo2.add1(1,2);
        //new
        Demo2 demo3 = new Demo2();
        int add2 = demo3.add(1,2);
        int add3 = demo3.add1(1,2);
        System.out.println(add);
        System.out.println(add1);
        System.out.println(add2);
    }

    //形式参数
    public static int add1(int a ,int b){
        return a+b;
    }
    public int add(int a ,int b){
        return a+b;
    }

}

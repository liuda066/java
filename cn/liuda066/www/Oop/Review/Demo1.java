package cn.liuda066.www.Oop.Review;
//方法回顾！！！

import java.io.IOException;

public class Demo1 {

    public static void main(String[] args) {
        //静态方法
        Student.say();
        //非静态方法 实例化 new
        Student student = new Student();
        student.say1();

    }


    //1.方法的定义

    /*
    修饰符 返回值类型 方法名（....）｛
        //方法体
        return 返回值；
    ｝
     */
    public String sayHello(){
        return "helloWorld!";
    }

    public void hello(){
        return ;
    }

    public int max (int a , int b){
        return a>b ?a:b;//三元运算符！
    }

    //break 跳出 switch 语句，结束整个循环。
    //return 返回值和返回值类型相同类型，结束方法，返回一个结果。
    //参数列表：（参数类型，参数名）...可变长参数（int...)
    //异常抛出，后面讲解

    //数组下标越界异常。


    public void readFile(String file) throws IOException {

    }

    //2.方法的调用：递归...

    /*
        静态方法 static

        非静态方法  需要实例化 使用new
     */

    //static 和 类一起加载
    public static void a(){
       // b();//调用失败，static 和 类一起加载，类实例化之后才存在
    }

    //类实例化之后才存在
    public void b(){

    }


}

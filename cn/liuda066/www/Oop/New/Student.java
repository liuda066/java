package cn.liuda066.www.Oop.New;

//学生类
public class Student {

    //属性 ： 字段
    String name;//null
    int age;//0

    //方法
    public void study(){
        System.out.println(this.name+"在学习！");
    }
}
//测试
/*public static void main(String[] args) {

        //类：抽象的，实例化
        //类实例化后会返回一个自己的对象！
        //Liuda066对象就是Student类的具体事例！
        Student Liuda066 = new Student();
        Student xiaowu = new Student();
        System.out.println(Liuda066.name);
        System.out.println(Liuda066.age);
        Liuda066.name ="六道牛牛";
        Liuda066.age =12;
        System.out.println(Liuda066.name);
        System.out.println(Liuda066.age);
        xiaowu.name ="小吴";
        xiaowu.age =13;
        System.out.println(xiaowu.name);
        System.out.println(xiaowu.age);
         }
        */
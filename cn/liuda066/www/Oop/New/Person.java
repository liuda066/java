package cn.liuda066.www.Oop.New;

/**
 * 构造器：
 *     1.和类名相同
 *     2.没有返回值
 * 作业：
 *      1.new 本质在调用构造方法
 *      2.初始化对象的值
 * 注意点：
 *      定义有参构造之后，如果像使用无参构造，必须显示定义一个无参构造
 *
 *      alt + Insert  快捷生成构造器
 *
 *      this. =  //代表当前类
 */
//java -->class
public class Person {
    //属性
    String name;
    int age;
//   一个类即使什么都不写，也会存在一个方法
    //显示的定义构造器

    //实例化初始值
    //1.使用new关键字，必须要有构造器，本质是在调用构造器
    //2.构造器用来初始化值
    public Person(){//无参构造 //默认

    }


    //有参构造:一旦定义了有参构造，无参构造就必须显示定义
    public Person(String name){//有参构造 //重载
        this.name = name;
    }

    //alt + insert 生成构造器
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

/*    public static void main(String[] args) {

        //new 实例化一个对象
        Person person = new Person();
        Person person1 = new Person("dasdas");
        System.out.println(person.name);//六道牛牛
        System.out.println(person1.name);//六道牛牛
    }*/

package cn.liuda066.www.Oop.New;

/**
 *      private : 私有
 *
 *
 */

//类
public class Demo3 {

    //属性私有

    private String name; //名字
    private int id;    //学号
    private char sex;    //性别
    private int age;     //年龄

    //提供一些可以操作这个属性的方法  get、set
    //提供一些public的 get，set方法

    //get 获得这个数据
    public String getName(){
        return this.name;
    }

    //set 给这个数据设置值
    public void setName(String name){
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 120 || age < 0) {//不合法
            this.age = 3 ;
        }else{
            this.age = age;
        }
    }

    //快捷键 alt + Insert

   /* public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }
*/

    //学习（）

    //睡觉（）


}

    /*public static void main(String[] args) {
        Demo3 student = new Demo3();
//        System.out.println(student.name);
        student.setName("六道牛牛");
        String name = student.getName();
        System.out.println(name);
        student.setAge( -99);
        System.out.println(student.getAge());
    }*/

/**
 * 封装详解
 *
 * 该漏的漏，该藏的藏
 *      追求“高内聚，低耦合”     高内聚：类的内部数据操作细节自己完成，不允许外部干涉
 *                              低耦合：仅暴露少量的方法给外部使用
 *
 *  封装（数据的隐藏）
 *      通常，应静止直接访问一个对象中数据的实际表示，而应通过接口来访问，这称为信息隐藏
 *
 *
 *  ---属性私有，get/set---
 *
 *  //手动写
 *  //快捷键 alt + Insert
 *
 *  封装的意义：
 *      1.提高程序的安全性，保护数据
 *      2.隐藏代码的实现细节
 *      3.同一接口（都是get.set)
 *      4.系统可维护性增加了
 *
 *
 *  判断方法是否相同：
 *          方法名，参数列表
 */

package cn.liuda066.www.Oop.New.Extends;

/**
 * 继承
 * 继承的本质是对某一批类的抽象，从而实现对现实世界更好的建模
 *
 * extends "扩展"。子类是父类的扩展
 *
 * JAVA中的类只有单继承，没有多继承 -->一个儿子只能有一个爸爸，但是一个爸爸可以有多个儿子
 *
 * 继承是类和类之间的一种关系。除此之外，类和类之间的关系还有倚赖，组合，聚合等。
 * 继承关系的两个类，一个为子类(派生类),一个为父类(基类)。子类继承父类，使用关键字extends表示。
 * 子类和父类之间，从意义上讲应该具有“is a" 的关系
 *
 * object类
 * super
 * 方法重写
 *
 * 子类可以继承父类的所有  非私有  方法和属性
 *
 *  快捷键 Ctrl + H
 */
public class Demo4 {
    public static void main(String[] args) {
        Student student = new Student();
        student.say();
        //System.out.println(student.money);
        Teacher teacher = new Teacher();
        teacher.say();
        //System.out.println(teacher.money);
        Person person = new Person();

    }
}

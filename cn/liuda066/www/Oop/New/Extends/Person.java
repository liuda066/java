package cn.liuda066.www.Oop.New.Extends;
//在JAVA中，所有的类，都默认直接或者间接 继承Object类
//Person 人 :父类
public class Person /*extends Object*/{

    //public  公共的
    //default 默认
    //protected 受保护的
    //private   私有的

    private int money = 10_0000_0000;
    public void say(){
        System.out.println("说了一句话");
    }


    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
